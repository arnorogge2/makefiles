#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <time.h> 
#define BUFF_SIZE 10240

int main() {

  printf("Content-type:text/html; charset=UTF-8\n\n");
  printf("<html>\n");
  printf("<body>\n");
  char * bufferPost;
  char categorie1[60], categorie2[60];
  char buff[BUFF_SIZE];
  size_t bufsize = 1024;
  bufferPost = (char * ) malloc(bufsize * sizeof(char));
  int i;
  time_t cTime;
  char c;
  cTime = time(NULL);
  char * t = ctime( & cTime);
  if (t[strlen(t) - 1] == '\n') 
    t[strlen(t) - 1] = '\0';

  FILE * f = fopen("/var/www/html/Website/char.json", "r+");
  fread(buff, BUFF_SIZE, 1, f);
  while (buff[strlen(buff) - 1] != ']') {
    buff[strlen(buff) - 1] = '\0';
  }
  buff[strlen(buff) - 1] = '\0';
  fclose(f);
  getline( & bufferPost, & bufsize, stdin);
  while (bufferPost[i] != '\0') {
    if (bufferPost[i] == '=') {
      bufferPost[i] = ' ';
    }
    if (bufferPost[i] == '&') {
      bufferPost[i] = ' ';
    }
    i++;
  }
  sscanf(bufferPost, "name %s race %s", & categorie1, & categorie2);
  sprintf(bufferPost, ",{\"name\":\"%s\",\"race\":\"%s\",\"time\":\"%s\"}]", categorie1, categorie2, t);
  strcat(buff, bufferPost);
  printf("%s", buff);
  FILE * f2 = fopen("/var/www/html/Website/char.json", "r+a");
  fprintf(f2, "%s", buff);
  fclose(f2);
  printf("<script>window.location.href = 'http://rpi-arnorogge/Website/index.html';</script>");
  printf("</body>\n");
  printf("</html>\n");
  return 0;
}